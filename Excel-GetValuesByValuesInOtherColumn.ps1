# find value in column2 by value in column1
# (offset (y, x) - (0,1))


$strPath="excel_spreadsheet.xlsx"
$objExcel=New-Object -ComObject Excel.Application
$objExcel.Visible=$false
$WorkBook=$objExcel.Workbooks.Open($strPath)
$worksheet = $workbook.sheets.item("sheet name") # sheet name
#$intRowMax = ($worksheet.UsedRange.Rows).count


foreach($user in Get-Content input_file_with_selected_values_in_column1.txt) {
    #$string = $user-replace '\s','' # strip entries from spaces
    $Searcher = $worksheet.usedrange.find($string)

    if ($Searcher) {
        # if value exists
        $target = $Searcher.offset(0,1)
        $nextCell = $target.text
        Write-Host "column1 value: $string, column2 value: $nextCell"
    } else {
        # if value doesn't exist
        Write-Host "column1 value: $string, column2 value: missing"
    }
}