Function Get-Drives
{
    $result = @()

    $ComputerName = Get-Content C:\temp\computers.txt
    $ErrorActionPreference = 'Stop'

    foreach ($Computer in $ComputerName)
    {
        
        Try
        {
            #$x = gwmi -class "Win32_NetworkAdapterConfiguration" -cn $Computer | ? IpEnabled -EQ "True" | select DNSHostName, MACAddress | FT -AutoSize
            $x = Get-WmiObject Win32_MappedLogicalDisk | select name, providername
            $paths = $x.providername
            $names = $x.name

            Write-Output "$Computer : responded"
            # foreach ($path in $paths)
            # {
            #     Write-Host "$path"
            # }
            # foreach ($name in $names)
            # {
            #     Write-Host "$name"
            # }

            foreach ($share in $x)
            {
                $share_name = $share.name
                $share_path = $share.providername
                Write-Host "$share_name = $share_path"
            }

            $result += $x
        }
        Catch
        {
            Write-Warning "$Computer : not reachable"
            $err = "$Computer : not reachable"
            $result += $err
        }

    }

    $result | Out-File c:\temp\output.txt  -width 120


}

Get-Drives