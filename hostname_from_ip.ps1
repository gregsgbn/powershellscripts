# ips.txt file should have one ip address per line

$results = @() 
 
Foreach ($c in Get-Content C:\temp\ips.txt) 
{ 
trap { continue; } 
$IPaddress = $c 
Write-Output "ip address: $c"
$FQDN = [System.Net.Dns]::GetHostEntry("$c")| Select-Object HostName 
 
$out = New-Object psobject 
$out | Add-Member -NotePropertyName IP $IPaddress 
$out | Add-Member -NotePropertyName DNS $FQDN 
 
$results += $out 
} 
 
$results | Out-File c:\temp\output2.txt  -width 120|Format-Table 