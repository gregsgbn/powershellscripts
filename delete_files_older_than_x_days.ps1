# examples:
#
# Delete files older than the $limit.
#Get-ChildItem -Path $path -Recurse -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit } | Remove-Item -Force
#
# Delete any empty directories left behind after deleting the old files.
#Get-ChildItem -Path $path -Recurse -Force | Where-Object { $_.PSIsContainer -and (Get-ChildItem -Path $_.FullName -Recurse -Force | Where-Object { !$_.PSIsContainer }) -eq $null } | Remove-Item -Force -Recurse

# https://www.shellandco.net/delete-the-recycle-bin-items-older-than-60-days/


$log = @()
Function Log($msg)
{
    Write-Output "$msg"
    $Global:log += "$msg`n" # `n newline
    #$global:log | Out-File $LogFilename -width 120 # write output file on any new log?
}




$dayLimit = 10
$date = Get-Date -format "yyyy-MM-dd_HHmmss"
$limit = (Get-Date).AddDays(-$dayLimit)
$path = "C:\temp\test"
$LogFilename = "c:\temp\DeleteOldFilesLog$date.txt"




Log("`n---------------------------------------------")
Log("script execution time: $date")
Log("----------------------------------------")
log("path: $path")
log("removing files older than $limit")
log("---------------------------------------------`n")


Function Delete-Old-Files
{
    $oldfiles = Get-ChildItem -Path $path -Recusre -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit }

    foreach ($file in $oldfiles)
    {
        Try
        {
            #$shell = new-object -comobject "Shell.Application" # with trash can - test
            #$item = $shell.Namespace(0).ParseName("$file")
            #$item.InvokeVerb("delete")

            $file | Remove-Item -Force

            
            Log("removed: $file")
        }
        Catch
        {
            Log("could not remove: $file")
        }
    }

}


Delete-Old-Files


$end_time = Get-Date -format "yyyy-MM-dd_HHmmss"
Log("`n----------------------------------")
Log("script finished: $end_time")
Log("----------------------------------")

$global:log | Out-File $LogFilename -width 120