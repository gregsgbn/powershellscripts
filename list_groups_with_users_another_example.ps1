#Get-WMIObject win32_group -filter "LocalAccount='True'" -computername $Server | Select PSComputername,Name,@{Name="Members";Expression={   $_.GetRelated("win32_useraccount").Name -join ";"  }}

$ComputerName=$env:COMPUTERNAME
[ADSI]$S = "WinNT://$computername"

$S.children.where({$_.class -eq 'group'}) |
Select @{Name="Computername";Expression={$_.Parent.split("/")[-1] }},
@{Name="Name";Expression={$_.name.value}},
@{Name="Members";Expression={
    [ADSI]$group = "$($_.Parent)/$($_.Name),group"
    $members = $Group.psbase.Invoke("Members")
    ($members | ForEach-Object { $_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null) }) -join ";"}}

