# PowerShell notebook #

### Running unsigned scripts ###
This will allow running unsigned scripts that you write on your local computer and signed scripts from Internet.
Start Windows PowerShell with the "Run as Administrator" option. Only members of the Administrators group on the computer can change the execution policy.
Enable running unsigned scripts by entering:
set-executionpolicy remotesigned

### Most useful examples ###
* $ComputerName=$env:COMPUTERNAME # get host computername
* $PSVersionTable.PSVersion # poweshell version
* PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& 'C:\Users\x875986\Documents\ps\getmac.ps1'" # run script from cmd (bat)
* Write-Output "$variable" # output examples

### Source for PowerShell script examples ###

* [gallery.technet.microsoft.com](https://gallery.technet.microsoft.com/scriptcenter/site/search?f%5B0%5D.Type=ProgrammingLanguage&f%5B0%5D.Value=PowerShell&f%5B0%5D.Text=PowerShell)
* [shellandco.net](https://www.shellandco.net/category/powershell/)
* [github.com/proxb](https://github.com/proxb/PowerShell_Scripts)

