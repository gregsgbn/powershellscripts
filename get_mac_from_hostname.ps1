# computers.txt - file with computer names (one computer hostname per line)

Function Get-Mac
{
    $result = @()

    $ComputerName = Get-Content C:\temp\computers.txt
    $ErrorActionPreference = 'Stop'

    foreach ($Computer in $ComputerName)
    {
        
        Try
        {
            $x = gwmi -class "Win32_NetworkAdapterConfiguration" -cn $Computer | ? IpEnabled -EQ "True" | select DNSHostName, MACAddress | FT -AutoSize

            Write-Output "$Computer : responded"
            #Write-Host "$Computer : responded"
            $result += $x
        }
        Catch
        {
            Write-Warning "$Computer : not reachable"
            $err = "$Computer : not reachable"
            $result += $err
        }

    }

    $result | Out-File c:\temp\output.txt  -width 120


}

Get-Mac