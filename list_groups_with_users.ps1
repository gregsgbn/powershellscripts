$date = Get-Date -format "yyyy-MM-dd_HHmmss"
$LogFilename = "c:\temp\GroupMembers_$date.txt"

$log = @()
Function Log($msg)
{
    Write-Output "$msg"
    $Global:log += "$msg`n" # `n newline
    #$global:log | Out-File $LogFilename -width 120 # write output file on any new entry
}


Trap {"Error: $_"; Break;} 
 
Function EnumLocalGroup($LocalGroup) 
{ 
    $Group = [ADSI]"WinNT://$strComputer/$LocalGroup,group" 
    $Members= @($Group.psbase.Invoke("Members")) # Invoke the Members method and convert to an array of member objects. 
    $memberCount = ($Members | measure-object).count
    
    Log("`n-----------------------`nGroup '$LocalGroup' ($memberCount users)" )
    if ($memberCount -gt 0)
    {
        Log("users:")
    }

 
    ForEach ($Member In $Members) 
    { 
        $Name = $Member.GetType().InvokeMember("Name", 'GetProperty', $Null, $Member, $Null) 
        Log("$Name")
    } 
} 
 

$strComputer = gc env:computername # Specify the computer. 
Log("`n`n---[ Computer: $strComputer ]---`n" )
$computer = [adsi]"WinNT://$strComputer"

$i=0 
foreach($adsiObj in $computer.psbase.children) 
{ 
    switch -regex($adsiObj.psbase.SchemaClassName) 
    { 
        "group"  
        {
            $group = $adsiObj.name 
            EnumLocalGroup $group
        }
    }
    $i++ 
}

$global:log | Out-File $LogFilename -width 120 # write log file